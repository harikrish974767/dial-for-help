/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.dial.help.registration.service.service;

import in.ac.gpckasaragod.dial.help.registration.service.model.ui.data.PersonalDetails;
import in.ac.gpckasaragod.dial.help.ui.model.ui.data.PersonalDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface PersonalDetailsService {

    public static String saveDailHelpDetailsForm(String Name, String Phone);

    public static String updatePersonalDetailsForm(Integer integer, String Name, String Phone);
    /*
    public static String saveDailHelpDetailsForm(String Name,Integer Phone);
    public static String updatePersonalDetailsForm(Integer integer, String Name, String Phone);   public static String saveDailHelpDetailsForm(String Name, String Phone);*/
   public String Dailhelp(Integer Id,String Name,Integer Phone);
   public PersonalDetails readPersonal(Integer Id);
   public List<PersonalDetails>getAllPersonalDetails();
   public String updatePersonalDetails(PersonalDetails Personal);
   public String updatePersonalDetails(Integer id,String name,Integer phone);
   public String deletePersonalDetails(Integer id);

 }

