/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.dial.help.registration.service.service.Impl;

import in.ac.gpckasaragod.dial.help.registration.service.model.ui.data.PersonalDetails;
import in.ac.gpckasaragod.dial.help.registration.service.service.PersonalDetailsService;
import in.ac.gpckasaragod.dial.help.ui.model.ui.data.PersonalDetails;
import java.awt.List;
import static java.lang.reflect.Array.get;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Name;
import static javax.swing.UIManager.get;

/**
 *
 * @author student
 */
public class PersonalDetailsServiceImpl extends ConnectionServiceImpl implements  PersonalDetailsService{

    private PersonalDetails personal;
    private PersonalDetails personalDetails;
    private Integer Phone;
    private String Name;
    private int Id;

    
  public String savePersonalDetailsService(String name,String phone){
      try{
         
      
      Connection connection = getConnection();
      Statement statement = connection.createStatement();
      String query = "INSERT INTO PERSONAL_DETAILS (NAME,PHONE) VALUES "+"('"+name+"','"+phone+"')";
      System.err.println("Query:"+query);
      int status = statement.executeUpdate(query);
      if (status ! = 1 ){
       return "Save failed";
  } else { 
      
          return"Saved successfully";
          
          }
      } catch (SQLException ex) {
          Logger.getLogger(PersonalDetailsService.class.getName()).log(Level.SEVERE,null,ex);
          return "Savefailed";
      }
  
  
 }  
  
 

public PersonalDetails readPersonalsDetails(Integer Id) throws SQLException {
    {
        PersonalDetails personalDetails = null;
        try {
        
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM PERSONAL_DETAILS WHERE ID=?";
    ResultSet resultSet = statement.executeQuery(query);
  while(resultSet.next()){
      Integer id =resultSet.getInt("ID");
      String Name = resultSet.getString("NAME");
      Integer Phone = resultSet.getInt("PHONE");

      personalDetails = new PersonalDetails(id,Name,Phone);
     
  }     
} catch(SQLException ex) {
    Logger.getLogger(PersonalDetailsService.class.getName()).log(Level.SEVERE,null,ex);
}
      return personalDetails;
    }
    
    
    @Override
    public List<PersonalDetails> getAllPersonalDetails(){
    List<PersonalDetails>  = new ArrayList<>();
    try{
    Connection connection = getConnection();
    Statement statement = connection.createStatement();
    String query = "SELECT * FROM PERSONAL_DETAILS";
    ResultSet resultSet = statement.executeQuery(query);
    
    
    while(resultSet.next()){
        int id = resultSet.getInt("ID");
        String Name =resultSet.getString("NAME");
        Integer Phone= resultSet.getInt("PHONE");
        personalDetails = new PersonalDetails(id,Name,Phone);
        personal.add(personalDetails);
    }
    
    
} catch(SQLException ex) {
    Logger.getLogger(PersonalDetailsService.class.getName()).log(Level.SEVERE,null,ex);
}
      return personal;
}
}
    @SuppressWarnings("empty-statement")
   public String UpdatePersonalDetails(Integer id = null,String name,Integer phone) {
    try{

    Connection connection = getConnection();
    Statement statement = connection.createStatement();
    String query = "UPDATE PERSONALDETAILS SET NAME ='"+Name+"',PHONE='"+Phone+"' WHERE=?";
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return"Updated failed";
        else
            return"Updated successfully";
    }catch(SQLException ex){
}

 
   
    @SuppressWarnings("empty-statement")
   public String deletePersonalDetails()
{
    try{

    Connection connection = getConnection();
    String query = "DELETE FROM PERSONALDETAILS WHERE ID =?";
    PreparedStatement statement = connection.prepareStatement(query);
    statement.setInt(1, Id);
    int delete = statement.executeUpdate();
    if(delete !=1)
        return "Delete failed";
    else
        return "Delete successfully";
}catch (SQLException ex) {
    return"Delete failed";
}



    

            


    

        
   