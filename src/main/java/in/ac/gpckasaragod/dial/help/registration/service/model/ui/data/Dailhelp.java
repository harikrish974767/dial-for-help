/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.dial.help.registration.service.model.ui.data;

import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class Dailhelp {
    private String name;
    private Integer phone;

    public Dailhelp(String name, Integer phone) {
        this.name = name;
        this.phone = phone;
    }
    private static final Logger LOG = Logger.getLogger(Dailhelp.class.getName());

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }
}
